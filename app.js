$(function() {
  $('#searchButton').click(function() {
    var searchTerm = $('#search').val();
    $.get('http://www.omdbapi.com/?s=' + searchTerm, displayResults);
  });
});

var moviesByID = {};

function displayResults(results) {
  var $movies = $('.movies');
  for (var i = 0; i < results.Search.length; i++) {
    var $p = $('<p data-imdbid="' + results.Search[i].imdbID + '">' + results.Search[i].Title + '</p>');
    $p.click(showMovieDetails);
    $movies.append($p);
    moviesByID[results.Search[i].imdbID] = results.Search[i];
  }
}

function showMovieDetails() {
  // console.log(this.dataset.imdbid);
  // for (var i = 0; i < allResults.length; i++) {
    // if(allResults[i].imdbID == this.dataset.imdbid) {
      // console.log(allResults[i]);
    // }
  // }
  console.log(moviesByID[this.dataset.imdbid]);
}

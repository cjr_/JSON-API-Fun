var getResults = resultGetter().getResults;

$(function() {
  $('#searchButton').click(function() {
    var searchTerm = $('#search').val();
    getResults(searchTerm)
      .then(showRandomMovie);
  });
});

function resultGetter() {
  var storedResults = null;

  return {
    getResults: function(searchTerm) {
      if(storedResults) {
        return Promise.resolve(storedResults);
      } else {
        return $.get('http://www.omdbapi.com/?s=' + searchTerm)
          .then(function(results) {
            storedResults = results.Search;
            return results.Search;
          });
      }
    }
  };
}

function showRandomMovie(results) {
  var randomIndex = Math.floor(Math.random() * (results.length - 1));
  var randomMovie = results[randomIndex];
  var $p = $('<p>' + randomMovie.Title + '</p>');
  $p.click(showAllMovies);
  $('.movies').append($p);
}

function showAllMovies() {
  getResults()
    .then(function(results) {
      console.log(results);
    });
}
